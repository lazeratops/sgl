#!/bin/sh
set -x;
echo "Building and installing SnailLife Genome Lab"
echo "Current dir" $PWD
go version

echo "Building SGL"
cd ../../../../../cmd/sgl;
go install

cd ../../
set -e

echo "Running tests"
CVPKG=$(go list ./... | grep -v -e mocks -e sgl | tr '\n' ',')
go test -race -v -coverpkg $CVPKG -coverprofile allcoverage-stable.out ./...
set +x