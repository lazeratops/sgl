package main

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	"log"
	"net/http"
	"os"
	"sgl/server"
	"sgl/server/env"
)

func main() {
	err := common.InitLog("")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	err = env.Congfigure(env.GetProjectRootPath() + "/server/config")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	r := server.NewRouter()

	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
