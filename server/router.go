package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"sgl/server/env"
	"sgl/server/handlers"
)

// TODO: put this into a config file
var SnailServeUrl = "http://localhost:49101"

// NewRouter creates a new router and registers all the routes
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	router.StrictSlash(true)

	router.PathPrefix("/static/").Handler(
		http.StripPrefix("/static", http.FileServer(http.Dir(env.GetProjectRootPath()+"/html/static/"))),
	).Methods("GET")

	templateHandler := handlers.Template{}
	err := templateHandler.Init(env.GetProjectRootPath())
	if err != nil {
		fmt.Println(err)
		return nil
	}
	router.Handle("/", templateHandler)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		router.
			Methods(route.Method, "OPTIONS").
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}
