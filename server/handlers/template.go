package handlers

import (
	"html/template"
	"log"
	"net/http"
	"regexp"
)

type Template struct {
	t *template.Template
}

func (h *Template) Init(projRoot string) (err error) {
	tmplPath := projRoot + "/templates/*"
	h.t, err = template.ParseGlob(tmplPath)
	if err != nil {
		log.Println("Cannot parse templates:", err)
		return err
	}
	return nil
}
func (h Template) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	routeMatch, _ := regexp.Compile(`^\/(\w+)`)
	matches := routeMatch.FindStringSubmatch(r.URL.Path)
	varmap := map[string]interface{}{}
	if len(matches) >= 1 {
		page := matches[1] + ".html"
		if h.t.Lookup(page) != nil {
			w.WriteHeader(200)
			h.t.ExecuteTemplate(w, page, varmap)
			return
		}
	} else if r.URL.Path == "/" {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(200)
		h.t.ExecuteTemplate(w, "index.html", varmap)
		return
	}
	w.WriteHeader(404)
	w.Write([]byte("NOT FOUND"))
}
