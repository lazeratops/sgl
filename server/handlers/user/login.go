package user

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/clientlib/user"
	"gitlab.com/drakonka/gosnaillife/common"
	user2 "gitlab.com/drakonka/gosnaillife/common/domain/user"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"io/ioutil"
	"net/http"
	"sgl/server/env"
	"time"
)

func LoginUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Login user")

	r.ParseForm()
	var username, password string
	if val, ok := r.Form["username"]; ok {
		username = val[0]
	}
	if val, ok := r.Form["password"]; ok {
		password = val[0]
	}
	res, err := user.Login(env.App.SnailLifeServerUrl, env.App.AuthProvider, username, password)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		msg := fmt.Sprintf("Error from SnailLife server: %v", err)
		w.Write([]byte(msg))
		return
	}
	if res.StatusCode != http.StatusOK {
		res.Body.Close()
		w.WriteHeader(res.StatusCode)
		body, _ := ioutil.ReadAll(res.Body)

		_, err = w.Write(body)
		if err != nil {
			common.Log.Error(err)
		}
		return
	}
	var user *user2.User
	user, err = restapi.GetUserFromUserRes(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		msg := fmt.Sprintf("Error when decoding user response: %v", err)
		w.Write([]byte(msg))
		return
	}

	fmt.Println("Setting cookie")
	// Finally, we set the client cookie for "token" as the JWT we just generated
	// we also set an expiry time which is the same as the token itself

	http.SetCookie(w, &http.Cookie{
		Name:    "username",
		Value:   username,
		Expires: time.Now().Add(60 * time.Minute),
	})
	for header, value := range user.AuthHeaders {
		http.SetCookie(w, &http.Cookie{
			Name:    header,
			Value:   value,
			Expires: time.Now().Add(60 * time.Minute),
		})
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}
